# ah_osconfig_ntp_nts

An Ansible role to configure NTP to use publicly-available NTS servers.

This role **does not** use my custom Ansible Vault credentials.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Fedora | Y | WORKING |
| Raspberry Pi OS | Y | WORKING |
| RHEL | Y | WORKING |
| Rocky Linux | Y | WORKING |
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-07-23 | Stop Chrony listening locally on udp/323 |
| 2022-01-03 | Update to allow use on Debian-based systems. |
| 2022-01-02 | First push, working with Chrony on Red Hat family distros. |
